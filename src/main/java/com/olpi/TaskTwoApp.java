package com.olpi;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class TaskTwoApp {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Calculator calculator = new Calculator();

        System.out.println("Please enter numbers!"
                + "\nPress \"ENTER\" after each number, "
                + "when you are finished enter \"OK\":");

        List<Integer> array = new ArrayList<>();

        while (scanner.hasNext()) {
            String initialText = scanner.nextLine();
            if (initialText.equalsIgnoreCase("ok")) {
                break;
            } else if (calculator.isInteger(initialText)) {
                array.add(Integer.valueOf(initialText));
            } else {
                System.out.println(
                        "You entered an invalid value, please try again!");
            }
        }

        if (!array.isEmpty()) {
            int minLength = calculator.findMinLength(array);
            System.out.println("Minimal length has number: " + minLength
                    + ", with length: "
                    + String.valueOf(Math.abs(minLength)).length());

            int maxLength = calculator.findMaxLength(array);
            System.out.println("Maximal length has number: " + maxLength
                    + ", with length: "
                    + String.valueOf(Math.abs(maxLength)).length());

            System.out.println(
                    "A number that has a minimal amount of different digits: "
                            + calculator.findMinimumDifferentDigits(array));

            System.out.print("A number that has increasing digits: ");
            Optional<Integer> value = calculator.findIncreasingDigits(array);

            if (value.isPresent()) {
                System.out.println(value.get());
            } else {
                System.out.println("Not found");
            }

            System.out.print("A number that has only unique digits: ");
            value = calculator.findOnlyUniqueDigits(array);
            if (value.isPresent()) {
                System.out.println(value.get());
            } else {
                System.out.println("Not found");
            }
        }
        scanner.close();
    }
}
