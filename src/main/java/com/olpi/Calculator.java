package com.olpi;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Calculator {

    public int findMinLength(List<Integer> array) {
        
        if(array.isEmpty()) {
            throw new IllegalArgumentException("Array is empty!");
        }

        int minLength = array.get(0);

        for (int i : array) {
            if (String.valueOf(Math.abs(i)).length() < String.valueOf(Math.abs(minLength))
                    .length()) {
                minLength = i;
            }
        }
        return minLength;
    }

    public int findMaxLength(List<Integer> array) {
        
        if(array.isEmpty()) {
            throw new IllegalArgumentException("Array is empty!");
        }

        int maxLength = array.get(0);

        for (int i : array) {
            if (String.valueOf(Math.abs(i)).length() > String.valueOf(Math.abs(maxLength))
                    .length()) {
                maxLength = i;
            }
        }
        return maxLength;
    }

    public int findMinimumDifferentDigits(List<Integer> array) {

        if(array.isEmpty()) {
            throw new IllegalArgumentException("Array is empty!");
        }
        
        List<Character> cache = new ArrayList<>();
        int amountOfDifferentDigits = 10;
        int desiredNumber = array.get(0);

        for (int i : array) {

            String checkedNumber = String.valueOf(Math.abs(i));
            cache.clear();
            int digitCounter = 0;

            for (int j = 0; j < checkedNumber.length(); j++) {
                if (!cache.contains(checkedNumber.charAt(j))) {
                    cache.add(checkedNumber.charAt(j));
                    digitCounter++;
                }
            }

            if (amountOfDifferentDigits > digitCounter) {
                amountOfDifferentDigits = digitCounter;
                desiredNumber = i;
            }
        }
        return desiredNumber;
    }

    public Optional<Integer> findIncreasingDigits(List<Integer> array) {
        
        boolean isIncreasing = false;

        for (int i : array) {

            int amountOfPositions = (int) Math.pow(10,
                    String.valueOf(Math.abs(i)).length() - 1.0);
            int checkedNumber = Math.abs(i);
            int previousDigit = checkedNumber / amountOfPositions;
            checkedNumber -= previousDigit * amountOfPositions;

            for (int position = amountOfPositions
                    / 10; position >= 1; position /= 10) {

                if (checkedNumber / position - previousDigit == 1) {
                    isIncreasing = true;
                    previousDigit = checkedNumber / position;
                    checkedNumber -= previousDigit * position;
                } else {
                    isIncreasing = false;
                    break;
                }
            }

            if (isIncreasing) {
                return Optional.of(i);
            }
        }
        return Optional.empty();
    }

    public Optional<Integer> findOnlyUniqueDigits(List<Integer> array) {
        
        boolean isUnique = false;
        List<Character> cache = new ArrayList<>();

        for (int i : array) {

            cache.clear();
            String checkedNumber = String.valueOf(i);

            for (int j = 0; j < checkedNumber.length(); j++) {

                if (!cache.contains(checkedNumber.charAt(j))) {
                    isUnique = true;
                    cache.add(checkedNumber.charAt(j));
                } else {
                    isUnique = false;
                    break;
                }
            }
            if (isUnique) {
                return Optional.of(i);
            }
        }
        return Optional.empty();
    }

    public boolean isInteger(String text) {
        try {
            Integer.parseInt(text);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

}
